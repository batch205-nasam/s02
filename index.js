//without the use of objects, our students from before would be organized as follows if we are to record additional information about them


// Spaghetti Code - When code is not organized enough that it becomes hard to work with.

// Encapsulation - Organize related information(properties) and behavior(method) to belong to a single entity

// Encapsulate the following info into 4 student objects using object literals:

/*
	Mini Activity

	Update the logout and listgrades methods of student 1
*/

// //create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


/* 
	QUIZ
	1. Spaghetti Code
	2. {}
	3. Encapsulation
	4. student1.enroll()
	5. True
	6. global object window
	7. true
	8. true
	9. true
	10.true
*/

// Function Coding

// function 2-4 - return

let student1 = {
	name: "John",
	email: "john@email.com",
	grades: [89, 84, 78, 88],
	login: function(){
		// login should allow our student object to display its own email with our message
		// what is 'this' in the context of an object method?
		// this, when inside a method, refers to the object it is in
		console.log(`${this.email} has logged in`)
	},
	logout: function(){
		console.log(`${this.email} has logged out`)
	},
	listGrades: function (){
		this.grades.forEach(grade => {
			console.log(grade);	
		})
	},
	computeAve: function(){
		let sum = 0;
		this.grades.forEach(function(grade){sum += grade});
		average = sum / this.grades.length;
			return average
	},
	willPass: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

	if(average >= 85){
		return true
	} else {
		return false
		}		
	},
	willPassWithHonors: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

		if(average >= 90){
			return true
		} else if(average >= 85 && average < 90 ) {
			return false
		} else if(average < 85) {
			return undefined
		}
	}
};

let student2 = {
	name: "Joe",
	email: "joe@email.com",
	grades: [78, 82, 79, 85],
	login: function(){
		console.log(`${this.email} has logged in`)
	},
	logout: function(){
		console.log(`${this.email} has logged out`)
	},
	listGrades: function (){
		this.grades.forEach(grade => {
			console.log(grade);	
		})
	},
	computeAve: function(){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;
		return average
	},
	willPass: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

	if(average >= 85){
		return true
	} else {
		return false
		}
	},
	willPassWithHonors: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

		if(average >= 90){
			return true
		} else if(average >= 85 && average < 90 ) {
			return false
		} else if(average < 85) {
			return undefined
		}
	}
};

let student3 = {
	name: "Jane",
	email: "jane@email.com",
	grades: [87, 89, 91, 93],
	login: function(){
		console.log(`${this.email} has logged in`)
	},
	logout: function(){
		console.log(`${this.email} has logged out`)
	},
	listGrades: function (){
		this.grades.forEach(grade => {
			console.log(grade);	
		})
	},
	computeAve: function(){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;
		return average
	},
	willPass: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

	if(average >= 85){
		return true
	} else {
		return false
		}
	},
	willPassWithHonors: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

		if(average >= 90){
			return true
		} else if(average >= 85 && average < 90 ) {
			return false
		} else if(average < 85) {
			return undefined
		}
	}
};

let student4 = {
	name: "Jessie",
	email: "jessie@email.com",
	grades: [91, 89, 92, 93],
	login: function(){
		console.log(`${this.email} has logged in`)
	},
	logout: function(){
		console.log(`${this.email} has logged out`)
	},
	listGrades: function (){
		this.grades.forEach(grade => {
			console.log(grade);	
		})
	},
	computeAve: function(){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;
		return average
	},
	willPass: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

	if(average >= 85){
		return true
	} else {
		return false
		}
	},
	willPassWithHonors: function(average){
	let sum = 0;
	this.grades.forEach(function(grade){sum += grade});
	average = sum / this.grades.length;

		if(average >= 90){
			return true
		} else if(average >= 85 && average < 90 ) {
			return false
		} else if(average < 85) {
			return undefined
		}
	}
};